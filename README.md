# Leech tool for NodeBB

Add a button to leech content from many sources and convert to NodeBB posts.

## Installation

    npm install gitlab:voanhcuoc/nodebb-plugin-leech-tool

## Development

_Note:_ `grunt` won't watch for json change, every time you change `plugin.json`
or language file, stop and start `grunt` again.
