const controllers = require('./controllers');
const leechWrapper = require('./leech');

function wrapper(Topics) {
  const leech = leechWrapper(Topics);
  function onAppLoad(params, initCallback) {
    const router = params.router;
    // app = params.app;
    const hostMiddleware = params.middleware;
    // const hostControllers = params.controllers;

    router.post('/api/leech-tool', (req, res) => {
      leech(req, (err, data) => {
        if (err) {
          // console.log(err);
          res.json({ err: err.message });
        } else {
          res.json(data);
        }
      });
    });

    // We create two routes for every view. One API call, and the actual route itself.
    // Just add the buildHeader middleware to your route and NodeBB will take care
    // of everything for you.

    router.get('/admin/plugins/quickstart', hostMiddleware.admin.buildHeader, controllers.renderAdminPage);
    router.get('/api/admin/plugins/quickstart', controllers.renderAdminPage);

    initCallback();
  }

  return onAppLoad;
}

module.exports = wrapper;
