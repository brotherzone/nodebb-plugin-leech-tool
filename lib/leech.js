const request = require('request');
// var session = require('express-session');
const parsers = require('../parser');

module.exports = Topics => (userRequest, callback) => {
  // request original-post then wrap and post it
  const url = userRequest.body.url;
  const uid = userRequest.uid;
  const cid = userRequest.body.cid;
  const tid = userRequest.body.tid;
  // console.log(url);
  if (!url || typeof url !== 'string') {
    callback(Error('[[error:invalid-url]]'));
  } else {
    request(url, (reqErr, res, raw) => {
      if (reqErr) {
        callback(reqErr);
      } else {
        const opt = { url, uid, cid, tid };
        const postData = Object.assign({}, parsers.try(raw, opt), { uid });
        if (!postData) {
          callback(Error('[[error:post-not-found]]'));
          return;
        }
        if (cid) {
          const newTopicData = Object.assign({}, postData, { cid });
          Topics.post(newTopicData, (postErr, data) => {
            callback(postErr, { tid: data.postData.tid });
          });
        } else if (tid) {
          postData.tid = tid;
          Topics.reply(postData, (postErr, data) => {
            callback(postErr, { pid: data.pid });
          });
        }
      }
    });
  }
};
