const Topics = module.parent.require('./topics');

const plugin = {};

plugin.init = require('./lib/init')(Topics);

plugin.defineWidgets = function defineWidgets(widgets, callback) {
  widgets.push({
    widget: 'leech-button',
    name: 'Leech Button',
    description: 'leech content from many sources and convert to NodeBB posts',
    content: '',
  });
  callback(null, widgets);
};

plugin.renderLeechButton = function renderLeechButton(widget, callback) {
  // console.log(widget.area.template);
  if (['category.tpl', 'topic.tpl'].includes(widget.area.template)) {
    widget.req.app.render('widgets/leech-button', (err, html) => {
      widget.html = html;
      callback(null, widget);
    });
  } else {
    widget.html = '';
    callback(null, widget);
  }
};

plugin.addAdminNavigation = function addAdminNavigation(header, callback) {
  header.plugins.push({
    route: '/plugins/quickstart',
    icon: 'fa-tint',
    name: 'Quickstart',
  });

  callback(null, header);
};

module.exports = plugin;
