const objectmodel = require('objectmodel');

const ObjectModel = objectmodel.ObjectModel;
// const ArrayModel = objectmodel.ArrayModel;

const vBulletinPost = new ObjectModel({
  threadname: String,
  username: String,
  userpage: String,
  userJoinDate: String,
  userNumOfPosts: String,
  postTimestamp: String,
  postContent: String,
});

module.exports = vBulletinPost;
