const _ = require('lodash');
const requireDirectory = require('require-directory');

const parsers = requireDirectory(module);
const models = require('../model');
const renderers = require('../renderer');

module.exports = {};

module.exports.try = (raw, opt) => {
  let modelled;
  let postData;
  let done;
  _.forOwn(parsers, (parser, name) => {
    // model name is from first char to the char before first '-'
    const modelName = name.split('-')[0];
    const model = models[modelName];
    const processed = parser.check(raw, opt);
    if (processed) {
      modelled = parser.parse(processed, opt);
      // console.log(modelled);
      if (model.test(modelled)) {
        // TODO get renderer base on settings
        const rendererName = `${modelName}-default`;
        const renderer = renderers[rendererName];
        postData = renderer.render(modelled, opt);
        // TODO test postdata model
        if (postData) {
          done = true;
          return false; // break
        }
      }
    }
    return true;
  });
  if (done) return postData;
  return undefined;
};
