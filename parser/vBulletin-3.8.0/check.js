const minify = require('html-minifier').minify;
const cheerio = require('cheerio');

module.exports = (raw) => {
  const minifiedHtml = minify(raw, {
    removeComments: true,
  });
  const $ = cheerio.load(minifiedHtml);
  const generator = $('head > meta[name="generator"]').attr('content');
  if (generator === 'vBulletin 3.8.0') {
    return $;
  }
  return undefined;
};
