const url = require('url');
const moment = require('moment');
// const S = require('string');
const _ = require('lodash');
const getConverter = require('cheerio-to-markdown');
const rulesWrapper = require('./rules');

function getUsernameAndUserpage(data) {
  const { $, link } = data;
  const result = data;
  const usernameTag = $('.bigusername')[0];
  if (!usernameTag) throw Error('[[error:invalid-post-url]]');
  result.username = $(usernameTag).html();
  result.userpage = `${link.protocol}//${link.hostname}/${$(usernameTag).attr('href')}`;
  // console.log(result.username, result.userpage);
  return result;
}

function getUserJoinDateAndUserNumOfPosts(data) {
  const { $ } = data;
  const result = data;
  const userinfoTag = $('td > div.smallfont:has(div)')[0];
  _.forEach(userinfoTag.children, (child) => {
    const text = $(child).text().trim();
    if (text.startsWith('Join Date:')) result.userJoinDate = text.split(' ').pop();
    if (text.startsWith('Posts:')) result.userNumOfPosts = text.split(' ').pop();
  });
  // console.log(result.userJoinDate, result.userNumOfPosts);
  return result;
}

function getThreadname(data) {
  const { $ } = data;
  const result = data;
  result.threadname = $($('td.tcat > .smallfont > a')[0]).text().trim();
  // console.log(result.threadname);
  return result;
}

function getTimestamp(data) {
  const { $ } = data;
  const timestamp = $($('td#currentPost > div.normal')[1]).text().trim();
  let tempdate;
  if (timestamp.includes('Yesterday')) {
    tempdate = moment().subtract(1, 'days').utc(-7).format('MM-DD-YYYY');
    timestamp.replace('Yesterday', tempdate);
  } else if (timestamp.includes('Today')) {
    tempdate = moment().utc(-7).format('MM-DD-YYYY');
    timestamp.replace('Today', tempdate);
  }
  const result = data;
  result.postTimestamp = timestamp;
  // console.log(data.postTimestamp);
  return result;
}

function getPostContent(data) {
  const { $, link } = data;
  const result = data;
  const rules = rulesWrapper({ host: link.host });
  $.prototype.toMarkdown = getConverter({
    ruleSets: [
      rules,
    ],
    condense: true,
  });
  result.postContent = $('td.alt1 > div.voz-post-message').toMarkdown();
  // console.log(data.postContent);
  // console.log(data);
  return result;
}

module.exports = ($, opt) => {
  // parse post page to model object
  const link = url.parse(opt.url);
  let data = { $, opt, link };
  data = getUsernameAndUserpage(data);
  data = getUserJoinDateAndUserNumOfPosts(data);
  data = getThreadname(data);
  data = getTimestamp(data);
  data = getPostContent(data);

  delete data.$;
  return data;
};
