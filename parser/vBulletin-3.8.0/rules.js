const url = require('url');
const _ = require('lodash');
const S = require('string');
const iconSource = require('./icon');

module.exports = opt => [
  {
    name: 'garbage/comment',
    filter: ':root',
    replacement(content, root, $) {
      const removeIfIsComment = function removeIfIsComment(node) {
        if (node.type === 'comment') {
          $(node).remove();
        }
      };

      $('*', root).toArray().forEach((node) => {
        removeIfIsComment(node);
        node.children.forEach((child) => {
          removeIfIsComment(child);
        });
      });
    },
  },
  {
    name: 'font-tag/color',
    filter: 'font[color]',
    replacement(content, node, $) {
      return `%(${$(node).attr('color').toLowerCase()})[${content}]`;
    },
  },
  {
    name: 'font-tag/size',
    filter: 'font[size]',
    replacement(content, node, $) {
      let level = 7 - parseInt($(node).attr('size'), 10);
      level = level === 7 ? 6 : level;
      $(node).replaceWith(`<h${level}>${content}</h${level}>`);
    },
  },
  {
    name: 'vbulletin/quote',
    filter: 'div[style]:has(table.voz-bbcode-quote td)',
    replacement(content, node, $) {
      const $td = $('td', node);
      const firstDiv = $td.children('div')[0];
      if (firstDiv && $(firstDiv).html().trim().startsWith('Originally Posted by ')) {
        // forum post quote
        const $titleDiv = $(firstDiv);
        const author = $titleDiv.children('strong').text();
        const link = $titleDiv.children('a').attr('href');
        // now second div is content-container
        const postContent = $($td.children('div')[1]).html();
        const authorTag = S(`Quote ${author}`).wrapHTML('b').s;
        const linkTag = S(`at %(gray)[${link || 'no link'}]`).wrapHTML('em').s;
        const contentTag = S(postContent).wrapHTML('blockquote').s;

        $(node).replaceWith(S(`${authorTag} ${linkTag} ${contentTag}`).wrapHTML('p').s);
      } else {
        // free quote
        // now td is content-container
        const postContent = $td.html();
        $(node).replaceWith(S(postContent).wrapHTML('blockquote').s);
      }
    },
  },
  {
    name: 'vbulletin/link',
    filter: 'a',
    replacement(content, node, $) {
      let link = $(node).attr('href');
      const linkObj = url.parse(link, true);

      // remove unnecessary 'amp;' in link
      if (link.host === opt.host) {
        link = link.replace(/amp;/g, '');
      }

      // remove vBulletin redirect
      if (link.startsWith('/redirect/index.php?link=')) {
        link = unescape(linkObj.query.link);
      }
      $(node).attr('href', link);

      // direct link without alt text
      // TODO: generalize this to all domain
      // if ($(node).html().trim().startsWith('https:\\/\\/vozforums.com')) {
      //   $(node).replaceWith(link);
      // }
    },
  },
  {
    name: 'vbulletin/icon',
    filter: 'img[src*="/images/smilies/"]',
    replacement(content, node, $) {
      const filename = _.last(S($(node).attr('src')).chompRight('.gif').s.split('/'));
      return `:${iconSource[filename]}:`;
    },
  },
  {
    name: 'invisible/hard-line-break',
    overwrite: 'commonmark/hard-line-break',
    replacement() {
      return '  \n';
    },
  },
];
