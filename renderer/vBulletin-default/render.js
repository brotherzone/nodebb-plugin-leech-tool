module.exports = (origData, opt) => {
  // build new topic data from original-post data
  const data = {};
  data.title = `${origData.threadname} by ${origData.username}`;
  data.tags = ['voz'];
  data.content = '\n' +
    `**[%(blue)[${origData.username}]](${origData.userpage})**\n` +
    `*Join date: ${origData.userJoinDate}*\n` +
    `*Number of posts: ${origData.userNumOfPosts}*\n` +
    `[**${origData.threadname}**](${opt.url})\n` +
    `Posted at: ${origData.postTimestamp}\n` +
    `\n---\n${origData.postContent}`;
  return data;
};
