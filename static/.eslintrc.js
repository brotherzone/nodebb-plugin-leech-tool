module.exports = {
  env: {
    browser: true,
    jquery: true,
  },
  rules: {
    'func-names': 'off',
    'prefer-arrow-callback': 'off',
    'strict': 'off',
    'prefer-template': 'off',
    'object-shorthand': 'off',
    'no-var': 'off',
  },
  globals: {
    'ajaxify': true,
    'app': true,
  }
}
