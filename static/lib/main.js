$(document).ready(function () {
  'use strict';

  $(window).on('action:ajaxify.end', function () {
    // console.log(info);
    var $button = $('#leech-button');
    var $dialog = $('#leech-dialog');
    $button.click(function () {
      $dialog.modal();
    });

    $('#leech-send').click(function () {
      var url = $('#leech-url').val();
      var template = ajaxify.data.template.name;
      var reqData = { url: url };

      if (template === 'category') {
        reqData.cid = ajaxify.data.cid;
      } else if (template === 'topic') {
        reqData.tid = ajaxify.data.tid;
      }

      // console.log(url);
      $.post('/api/leech-tool', reqData, function success(resData) {
        var dest;
        if (resData.tid) {
          dest = 'topic/' + resData.tid;
        } else if (resData.pid) {
          dest = 'post/' + resData.pid;
        } else {
          app.alert({
            alert_id: 'leech-failed',
            type: 'danger',
            title: 'Failed',
            message: 'Post not found',
          });
          return;
        }
        $dialog.one('hidden.bs.modal', function (/* err */) {
          ajaxify.go(dest);
        });
        $dialog.modal('hide');
      });
    });
  });
});
