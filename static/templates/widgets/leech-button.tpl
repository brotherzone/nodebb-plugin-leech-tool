<button id="leech-button" class="btn btn-success">
  [[leechtool:button.label]]
</button>

<div id="leech-dialog" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">[[leechtool:modal.title]]</h4>
      </div>
      <div class="modal-body">
        <form action="#">
          <div class="form-group">
            <label for="leech-url">[[leechtool:modal.input-url.label]]: </label>
            <input class="form-control" type="text" id="leech-url">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">[[leechtool:modal.button-close]]</button>
        <button id="leech-send" type="button" class="btn btn-primary">[[leechtool:modal.button-leech]]</button>
      </div>
    </div>
  </div>
</div>
