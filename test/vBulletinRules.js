const chai = require('chai');
const cheerio = require('cheerio');
const getConverter = require('cheerio-to-markdown');
const vBulletinRules = require('../lib/vBulletinRules');

chai.should();

function match(html, md, msg) {
  const $ = cheerio.load(html);
  $.prototype.convert = getConverter({
    condense: true,
    ruleSets: [
      vBulletinRules,
    ],
  });
  const main = $('body')[0];
  it(msg || '> should return markdown with appropriate content', () => {
    const result = $(main).convert();
    result.should.equal(md);
  });
}

describe('vBulletinRules', () => {
  describe('/garbage/comment', () => {
    const html = '<p>This is a <!-- comment --></p>';
    const md = '\nThis is a\n';
    match(html, md);
  });
});
